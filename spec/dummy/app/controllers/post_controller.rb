class PostController < ApplicationController
  include MyGem::ViewHelpers::ActionView
  def index
    @my_gem= MyGem::ViewHelpers::ActionView::Greet.new 'Bob'
    @message = @my_gem.message
  end
  helper_method :new_method_from_gem, :link_to_name
end
