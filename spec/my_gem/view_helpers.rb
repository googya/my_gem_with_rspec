require 'rails_helper'

describe MyGem::ViewHelpers::Actioniew do
  let(:test_class) { Struct.new(:my_gem) { include MyGem::ViewHelpers::ActionView } }
  let(:my_gem) { test_class.new }


  describe 'Greet' do
    let(:name) { 'Bob' }

    before(:all) do
      let(:greet) { Greet.new { include MyGem::ViewHelpers::ActionView }}
      @greet = MyGem::ViewHelpers::ActionView::Greet.new(:name)
    end

    describe '#message' do
      it 'returns' do
        expect(greet.message).to eql "Hi #{:name}"
      end
    end
  end


  describe 'new_method_from_gem' do
    it 'returns hallo World' do
      expect(my_gem.new_method_from_gem).to eql 'Hello World'
    end
  end

  describe 'link_to_status' do
    it 'span link' do
      my_gem.extend ActionView::Helpers::TagHelper
      expect(my_gem.link_to_name).to eq '<span>My Gem</span>'
    end
  end

end
