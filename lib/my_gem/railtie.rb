require 'my_gem'
module MyGem
  class Railtie < ::Rails::Railtie
    initializer "my_gem.configure_view_controller" do |app|
      ActiveSupport.on_load :action_view do
        require 'my_gem/view_helpers/action_view'
        include MyGem::ViewHelpers::ActionView
      end
    end
  end
end
